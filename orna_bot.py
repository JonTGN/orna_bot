# install : opencv-python
#           numpy
#           python3_xlib
#           pyautogui

import cv2
import imutils as imutils
import numpy as np
from PIL import ImageGrab, ImageOps
from numpy import *
import pyautogui
import time
import glob
import random
from matplotlib import pyplot as plt, image

timeSleep = 1
deadly_monsters = [""]

class Coordinates:

    potionBtn = (1840, 990)
    autoHealBtn = (960, 600)
    closePotionBagBtn = (950, 1030)
    continueBtn = (960, 920)  # After item pickup
    torch = (1150, 800)
    battleBtn = (1100, 950)
    mainAttackBtn = (random.randrange(240, 260), random.randrange(790, 810))

def heal():

    print("healing player...")
    pyautogui.click(Coordinates.potionBtn)
    time.sleep(timeSleep)
    pyautogui.click(Coordinates.autoHealBtn)
    time.sleep(timeSleep)
    pyautogui.click(Coordinates.closePotionBagBtn)

def torch():

    print("running torch")
    print()
    print()
    pyautogui.click(Coordinates.potionBtn)
    time.sleep(timeSleep)
    pyautogui.scroll(-100)
    time.sleep(timeSleep)
    pyautogui.click(Coordinates.torch)
    time.sleep(timeSleep + 0.5)
    pyautogui.click(Coordinates.closePotionBagBtn)

def attack():
    pyautogui.click(Coordinates.battleBtn)

    dead = False
    loopCounter = 0
    while dead is False:
        if loopCounter == 10:
            orna_bot()
        else:
            time.sleep(timeSleep)
            print("attack")
            pyautogui.click(random.randrange(240, 260), random.randrange(790, 810))
            time.sleep(timeSleep + 3)
            pyautogui.click(280, 600)
            death = isDead()
            if death is True:
                print('monster/player is dead')
                dead = True
                pyautogui.click(Coordinates.continueBtn)
                time.sleep(timeSleep)
                if loopCounter == 3:
                    heal()
                    continue
            loopCounter += 1
            continue


def isDead():
    print("is dead?")
    screen = (0, 0, 1920, 1080)
    location = (0, 0)
    ImageGrab.grab(screen).save("screenshots/screenshot.jpg")
    time.sleep(1)
    img_rgb = cv2.imread('screenshots/screenshot.jpg')
    img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)
    files = glob.glob('buttons/*.png')

    death = False
    for file in files:
        location = find_sprite(file)
        if location != (0, 0):
            print("button found at: ", location)
            print()
            print()
            print("file is: ", file)

            if file == 'buttons\continue.png':
                death = True
                pyautogui.click(location)
                print("monster dead")
                return death

            elif file == 'buttons\leave.png':
                pyautogui.click(location)
                print("player is dead, preforming heal, adding monster to list")
                deadly_monsters.append(file)
                pyautogui.click(location)
                time.sleep(timeSleep)
                heal()
    return death
            # if victory: monster dead, return true
            # elif leave: player dead, heal, add monster to do not attack list, return true
'''
            pyautogui.click(location)
            time.sleep(timeSleep)

    # template = cv2.imread('buttons/continue.png', 0)

    w, h = template.shape[::-1]
    res = cv2.matchTemplate(img_gray, template, cv2.TM_CCOEFF_NORMED)
    threshold = 0.6
    loc = np.where(res >= threshold)

    death = False
    for pt in zip(*loc[::-1]):
        cv2.rectangle(img_rgb, pt, (pt[0] + w, pt[1] + h), (0, 255, 255), 2)
        death = True
    return death
'''

def find_sprite(template):

    sprite_location = (0, 0)

    # Read the main image
    img_rgb = cv2.imread('screenshots/screenshot.jpg')

    # Convert to grayscale
    img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)

    # Read the template
    template = cv2.imread(template, 0)

    # Store width and height of template in w and h
    w, h = template.shape[::-1]

    res = cv2.matchTemplate(img_gray, template, cv2.TM_CCOEFF_NORMED)
    threshold = 0.85
    loc = np.where(res >= threshold)

    for pt in zip(*loc[::-1]):
        cv2.rectangle(img_rgb, pt, (pt[0] + w, pt[1] + h), (0, 255, 255), 2)
        print("image found")
        sprite_location = (pt[0] + 15, pt[1] + 15)
        return sprite_location
    return sprite_location
    # cv2.imshow('Detected', img_rgb)
    # cv2.waitKey(0)


def orna_bot():

    # pyautogui.click(930, 515)
    files = glob.glob('monsters/*.png')
    location = (0, 0)
    deadly_monsters = [""]

    while True:
        screen = (0, 0, 1920, 1080)
        ImageGrab.grab(screen).save("screenshots/screenshot.jpg")
        time.sleep(timeSleep)
        torch()

        i = 0
        j = 0
        while j < 50:
            sprite = False  # If the bot did not find a monster, continue button must be in the way
            while i < 11:
                ImageGrab.grab(screen).save("screenshots/screenshot.jpg")
                for file in files:
                    for m in deadly_monsters:
                        if file == deadly_monsters:
                            print("skipped monster")
                            continue

                        print("trying to find sprite.. ", file)
                        location = find_sprite(file)
                        if location != (0, 0):
                            sprite = True
                            print("image location is ", location)
                            print()
                            print()
                            pyautogui.click(location)
                            time.sleep(timeSleep)
                            isDead()
                            attack()
                            time.sleep(timeSleep)
                            ImageGrab.grab(screen).save("screenshots/screenshot.jpg")
                            # if isDead():
                            #     pyautogui.click(Coordinates.continueBtn)
                            #     heal()
                            #     deadly_monsters.append(file)
                            # time.sleep(timeSleep)
                if sprite is not True:  # If there is no sprite found, press continue button
                    pyautogui.click(Coordinates.continueBtn)
                if i == 4:
                    print("i is ", i)
                    print("healing")
                    heal()
                    i = 0
                i += 1
                j += 1
                if j == 2:
                    orna_bot()

    # implement method to store which monster killed you and skip over it in find sprite
    # implement method to check if you're dead or not at beginning of find sprite?

    # find_sprite()
    # time.sleep(timeSleep)
    # heal()
    # torch()


time.sleep(timeSleep)
orna_bot()

